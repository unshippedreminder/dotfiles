-- Setting Airline theme
vim.g['airline_theme'] = 'base16_adwaita'
-- Vimscript: `let g:airline_theme='base16_adwaita'`
vim.g['airline_powerline_fonts'] = 1

vim.opt.number = true -- Enable line numbers in the left margin
-- Vimscript: `set number`
vim.opt.cursorline = true -- highlight current cursorline

-- Show whitespaces
vim.opt.listchars = 'tab:>-,trail:~,extends:>,precedes:<'
-- Vimscript: `set listchars=tab:>-,trail:~,extends:>,precedes:<`
vim.opt.list = true

-- Tabs
vim.opt.expandtab = true -- Spaces instead of TAB
vim.opt.tabstop = 2 --  number of columns occupied by a tab 
vim.opt.softtabstop = 2 -- see multiple spaces as tabstops so 
vim.opt.shiftround = true -- round indentation to multiples of 'shiftwidth' when shifting text
vim.opt.autoindent = true -- reproduce the indentation of the previous line
vim.opt.shiftwidth = 2 -- Width for autoindents

-- Search
vim.opt.hlsearch = true -- Hightlight search
vim.opt.incsearch = true -- Increment search

-- Mouse
vim.opt.mouse = 'a' -- Enable mouse clicks
vim.opt.cursorline = true -- Highlight current cursorline

-- Start with cursor in file editing window
-- vim.cmd([[autocmd VimEnter * wincmd w]]) -- Doesn't work

-- Skeleton Files
-- Systemd
vim.api.nvim_create_autocmd({'BufNewFile'},
  {
    pattern = '*.service',
    command = string.format([[0r %s]], vim.fn.expand('~/.config/nvim/templates/skeleton.service'))
  }
)
vim.api.nvim_create_autocmd({'BufNewFile'},
  {
    pattern = '*.timer',
    command = string.format([[0r %s]], vim.fn.expand('~/.config/nvim/templates/skeleton.timer'))
  }
)

-- Quadlet
vim.api.nvim_create_autocmd({'BufNewFile'},
  {
    pattern = '*.container',
    command = string.format([[0r %s]], vim.fn.expand('~/.config/nvim/templates/skeleton.container'))
  }
)
vim.api.nvim_create_autocmd({'BufNewFile'},
  {
    pattern = '*.network',
    command = string.format([[0r %s]], vim.fn.expand('~/.config/nvim/templates/skeleton.network'))
  }
)
vim.api.nvim_create_autocmd({'BufNewFile'},
  {
    pattern = '*.volume',
    command = string.format([[0r %s]], vim.fn.expand('~/.config/nvim/templates/skeleton.volume'))
  }
)
vim.api.nvim_create_autocmd({'BufNewFile'},
  {
    pattern = '*.kube',
    command = string.format([[0r %s]], vim.fn.expand('~/.config/nvim/templates/skeleton.kube'))
  }
)

-- Programming
vim.api.nvim_create_autocmd({'BufNewFile'},
  {
    pattern = '*.sh',
    command = string.format([[0r %s]], vim.fn.expand('~/.config/nvim/templates/skeleton.sh'))
  }
)
vim.api.nvim_create_autocmd({'BufNewFile'},
  {
    pattern = '*.py',
    command = string.format([[0r %s]], vim.fn.expand('~/.config/nvim/templates/skeleton.py'))
  }
)
