aliases |= {
    # ls
    'ls': 'eza --long --group-directories-first --git --header --icons --classify',
    'la': 'eza --long --group-directories-first --git --header --icons --classify --all',
    'lt': 'eza --long --group-directories-first --git --header --icons --classify --tree --level 5',
    # Run in Toolbox
    'tldr': 'toolbox run --container fedora-toolbox-custom tldr',
    'pip': 'toolbox run --container fedora-toolbox-custom pip',
    # cd
    '..': 'z ..',
    '...': 'z ../..',
    '....': 'z ../../..',
    'cd': 'z',
    'z': 'zoxide',
    # find > fd
    'find': 'fd',
    # Toolbox
    'tr': 'toolbox run',
    'te': 'toolbox enter',
    'tc': 'toolbox create',
    # Distrobox
    'd': 'distrobox',
    'de': 'distrobox enter',
    'dc': 'distrobox create',
    # Ansible
    'ap': 'ansible-playbook',
    'ag': 'ansible-galaxy',
    # RPM-OSTree
    'rot': 'rpm-ostree',
    'roti': 'rpm-ostree install',
    'rotu': 'rpm-ostree upgrade',
    'rotr': 'rpm-ostree remove',
    # ripgrep
    'rg': 'rg --smart-case',
    # less > most
    'less': 'most',
}
