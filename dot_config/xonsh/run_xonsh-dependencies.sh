#!/usr/bin/bash

echo "Updating dependencies for Xonsh"
toolbox run pip install --upgrade  xontrib-argcomplete \
  xontrib-prompt-starship \
  xontrib-ssh-agent \
  jedi \
  > /dev/null
