set -g mouse on
set-option -g prefix C-a

# Set Xonsh as default shell in Tmux
set-option -g default-shell "/usr/bin/xonsh"

# Set statusbar from vim-airline & tmuxline.vim
source-file ~/.config/tmux/airline-theme.tmux

# Recommended by LazyVim
set-option -sg escape-time 10
set-option -g focus-events on
set-option -sa terminal-features ',xterm-256color:RGB'

## List of plugins
# tmux Plugin Manager
set -g @plugin 'tmux-plugins/tpm' # https://github.com/tmux-plugins/tpm
# Sensible defaults for tmux
set -g @plugin 'tmux-plugins/tmux-sensible' # https://github.com/tmux-plugins/tmux-sensible

# Automatic save and restore of tmux session
set -g @plugin 'tmux-plugins/tmux-resurrect' # https://github.com/tmux-plugins/tmux-resurrect
set -g @plugin 'tmux-plugins/tmux-continuum' # https://github.com/tmux-plugins/tmux-continuum

# Suspend tmux with F12, to allow nested sessions (eg. on remote hosts)
set -g @plugin 'MunifTanjim/tmux-suspend' # https://github.com/MunifTanjim/tmux-suspend

# Show tmux mode in status line
# Also shows if tmux is suspended (tmux-suspend)
set -g @plugin 'MunifTanjim/tmux-mode-indicator' # https://github.com/MunifTanjim/tmux-mode-indicator

# Plugin for copying to system clipboard
set -g @plugin 'tmux-plugins/tmux-yank' # https://github.com/tmux-plugins/tmux-yank
## List of plugins END

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.config/tmux/plugins/tpm/tpm'
